const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Card = new Schema({
    name:{
        type:String,
        default:"Name not specified"
    },
    nationality:{
        type:String,
        default:"Nacionality not specified"
    },
    gener:{
        type:String,
        default:"Gender not specified"
    },
    dob:{
        type: Date,
        default:"Date of Birth not specified"
    },
    physicalCharacteristics:[{
        height:{
            type:Number,
            default:"Height not specified"
        },
        hairColor:{
            type:String,
            default:"Hair Color not specified"
        },
        eyeColor:{
            type:String,
            default:"Eye Color not specified"
        }
    }],
    socialNetowrk:[{
        faceBook:{
            type:String,
            default:"Facebook profile not found"
        },
        instagram:{
            type:String,
            default:"Instagram profile not found"
        },
        twitter:{
            type:String,
            default:"Twitter profile not found"
        }
    }],
    processes:[{
        process:{
            title:String,
            link:String,
            type:String,
            default:"No process found"
        }
    }],
    sitesQuotes:[{
        quote:{
            title:String,
            link:String,
            type:String,
            default:"No site with quotes found"
        }
    }],
    fileWithMoreDatas:{
        type:String,
        default:"File with more data not sent"
    }

});

mongoose.model("Card",Card);