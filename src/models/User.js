const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const User = new Schema({
    name: {
        type:string,
        required:true
    },
    password: {
        type:string,
        required:true
    }
});

mongoose.model('User', User);