function createLabel(event) {

	var chave = document.getElementById('key');
	var valor = document.getElementById('value');

	if (event.keyCode === 13) {
		var newLine = document.createElement('br');

		var Label1 = document.createElement('label');
		Label1.setAttribute('contenteditable', 'true');
		Label1.setAttribute('class', 'label font-weight-bold');
		Label1.innerText = chave.value

		var Label2 = document.createElement('label');
		Label2.setAttribute('contenteditable', 'true');
		Label2.setAttribute('class', 'label');
		Label2.innerText = valor.value;

		document.getElementById('addInfo').appendChild(Label1);
		document.getElementById('addInfo').appendChild(Label2);
		document.getElementById('addInfo').appendChild(newLine);

		chave.value = '';
		valor.value = '';
	}

}

function searchCard() {
	var input, filter, table, tr, td, i, txtValue;
	input = document.getElementById("searchCard");
	filter = input.value.toUpperCase();
	table = document.getElementById("tableCards");
	tr = table.getElementsByTagName("tr");

	for (i = 0; i < tr.length; i++) {
		td = tr[i].getElementsByTagName("td")[0];
		if (td) {
			txtValue = td.textContent || td.innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				tr[i].style.display = "";
			} else {
				tr[i].style.display = "none";
			}
		}
	}
}

function sortTable(n) {
	var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	table = document.getElementById("tableCards");
	switching = true;

	dir = "asc";

	while (switching) {
		switching = false;
		rows = table.rows;

		for (i = 1; i < (rows.length - 1); i++) {
			shouldSwitch = false;

			x = rows[i].getElementsByTagName("TD")[n];
			y = rows[i + 1].getElementsByTagName("TD")[n];

			if (dir == "asc") {
				if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
					shouldSwitch = true;
					break;
				}
			} else if (dir == "desc") {
				if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
					shouldSwitch = true;
					break;
				}
			}
		}
		if (shouldSwitch) {
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
			
			switchcount++;
		} else {
			if (switchcount == 0 && dir == "asc") {
				dir = "desc";
				switching = true;
			}
		}
	}
}