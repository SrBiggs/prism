const express = require("express");
const app = express(); 
const BodyParser = require("body-parser");
const morgan = require("morgan");
const mongo = require("mongoose");
const util = require('util');

app.set("views",__dirname+"/src/views");
app.set("view engine","ejs");

app.use(express.static(__dirname + "/src/public"));
app.use(BodyParser.urlencoded({extended:true}));
app.use(morgan("short")); // set combined when running on Web server

// Connection Database
// mongo.Promise = global.Promise;
// mongo.connect("mongodb://localhost/PRISM").then(() => {
//     console.log("[+] Connected to Database");
// }).catch((err) => {
//     console.log("[!] Error connecting to Database"+err);
// });


app.get("/",function(req,res){
    res.redirect("/home");
});

app.get("/login",function(req,res){
    res.render("login");
});
app.get("/home",function(req,res){
    // var login = req.body;
    var teste = 'view()';
    var data_people = [
        {"name":"Name","value":"Vinicius"},
        {"name":"CPF","value":"000.000.000-00"},
        {"name":"RG","value":"00.000.000-0"},
        {"name":"Nationality","value":"Brasileiro"},
        {"name":"Cidade","value":"São Paulo"},
        {"name":"Estado","value":"SP"},
        {"name":"Gener","value":"Masculino"},
        {"name":"DOB","value":"12-03-1980"},
    ]
    res.render('home',{
        personalData:data_people
    });
      
});
app.get("/getdata",function(req,res){
   res.render("get_data");
});
app.get("/system", function(req,res){
    res.render("server");
});
app.get("/view", function(req,res) {
    var physicalCharacteristics = [
        {"name":"height","value":"1.82"},
        {"name":"hairColor","value":"Preto"},
        {"name":"eyeColor","value":"Preto"}
    ]
    var data_people = [
        {"name":"idCard","value":"000908324"},
        {"name":"Name","value":"Vinicius"},
        {"name":"CPF","value":"000.000.000-00"},
        {"name":"RG","value":"00.000.000-0"},
        {"name":"Nationality","value":"Brasileiro"},
        {"name":"Cidade","value":"São Paulo"},
        {"name":"Estado","value":"SP"},
        {"name":"Gener","value":"Masculino"},
        {"name":"DOB","value":"12-03-1980"},
    ]
   res.render('viewCard',{
        physicalCharacteristics:physicalCharacteristics,
        personalData:data_people
    }); 
});
app.get("/edit",(req,res) => {
    var data_people = [
        {"name":"idCard","value":"000908324"},
        {"name":"Name","value":"Vinicius"},
        {"name":"CPF","value":"000.000.000-00"},
        {"name":"RG","value":"00.000.000-0"},
        {"name":"Nationality","value":"Brasileiro"},
        {"name":"Cidade","value":"São Paulo"},
        {"name":"Estado","value":"SP"},
        {"name":"Gener","value":"Masculino"},
        {"name":"DOB","value":"12-03-1980"},
    ]

    var physicalCharacteristic = [
        {"name":"height","value":"1.82"},
        {"name":"hairColor","value":"Preto"},
        {"name":"eyeColor","value":"Preto"}
    ]
    res.render('editCard',{
        personalData:data_people,
        physicalCharacteristics:physicalCharacteristic,
    });
});

app.listen(8080, ()=> {
    console.log("Server running on http://localhost:8080");
});